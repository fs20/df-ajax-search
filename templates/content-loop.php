<?php
/**
 * @var WP_Post $post
 * @var $this \DF_AjaxSearch\App
 */
if (!defined('ABSPATH')) { die('Permission denied'); }
if (!$avatar = get_post_meta($post->ID, '_company_avatar', true)) {
    $avatar = $avatar = $this->_get_image_url('default-logo.png');
}
?>
<a href="<?php echo get_the_permalink($post->ID); ?>" class="df-item">
    <?php /* <img src="<?php echo $avatar; ?>" class="df-thumbnail" /> */ ?>
    <div class="info-block">
        <div class="title"><?php echo $post->post_title; ?></div>
        <div class="categories"><?php echo $this->_get_terms_list($post) ?></div>
    </div>
</a>
