<?php
/**
  Plugin Name: DF AjaxSearch
  Description: ajax search results
  Version: 1.0
  Author: Andrii Chaikovskyi
  Author URI: http://dev-force.com
  License: GPLv2
 */
define('DF_AJAXSEARCH_VERSION', '1.0.1');
spl_autoload_register('df_ajax_search_autoload');
function df_ajax_search_autoload($class) {
    $file_path = __DIR__.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.str_replace('\\', DIRECTORY_SEPARATOR, str_replace('DF_AjaxSearch', '', $class)).'.php';
    if (file_exists($file_path)) {
        require_once($file_path);
    }
}
$config = [
    'plugin_dir' => plugin_dir_path(__FILE__),
    'base_name' => plugin_basename(__FILE__),
];
\DF_AjaxSearch\App::run($config);