<?php
namespace DF_AjaxSearch;

/**
 * Class App (Singleton)
 *
 * @package DF_Ajax_Search
 */
class App {
    /**
     * Application instance
     *
     * @var App|null
     */
    protected static $_instance = null;
    protected $_plugin_dir = null;
    protected $_plugin_basename = null;
    /** @var $_adapter \wpdb */
    protected $_adapter = null;

    protected function __construct() {}
    protected function __clone() {}


    /**
     * Retrieving application instance
     *
     * @return App|null
     */
    public static function get_instance()
    {
        if (!self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * Initializing required params
     *
     * @param array $config
     * @throws \Exception
     */
    protected static function _init($config)
    {
        global $wpdb;
        self::$_instance->_adapter = $wpdb;
        if (empty($config['plugin_dir'])) {
            throw new \Exception('DF AjaxSearch plugin_dir not defined');
        }
        if (empty($config['base_name'])) {
            throw new \Exception('DF AjaxSearch base_name not defined');
        }
        self::$_instance->_plugin_dir = $config['plugin_dir'];
        self::$_instance->_plugin_basename = $config['base_name'];
    }

    /**
     * Running application
     *
     * @param array $config
     */
    public static function run(array $config)
    {
        self::get_instance();
        self::_init($config);
        add_action('wp_enqueue_scripts', array(self::get_instance(), 'register_assets'));
        add_action('wp_ajax_df_search', array(self::get_instance(), 'search'));
        add_action('wp_ajax_nopriv_df_search', array(self::get_instance(), 'search'));
    }

    /**
     * Registering required assets (JS/CSS)
     */
    public function register_assets()
    {
        wp_register_style('df-ajaxsearch', self::get_instance()->get_css_url('df_ajaxsearch'), array(), DF_AJAXSEARCH_VERSION);
        wp_enqueue_style('df-ajaxsearch', self::get_instance()->get_css_url('df_ajaxsearch'), array(), DF_AJAXSEARCH_VERSION);

        wp_register_script('df-ajaxsearch', self::get_instance()->get_js_url('df_ajaxsearch'), array('jquery'), DF_AJAXSEARCH_VERSION, true);
        wp_localize_script('df-ajaxsearch', 'df_ajax_search', ['selector' => 'form #search_keywords', 'ajaxurl' => admin_url('admin-ajax.php')]);
        wp_enqueue_script('df-ajaxsearch');
    }

    /**
     * Retrieving fields mapping
     *
     * @return array
     * @todo move to options
     */
    protected function _get_fields_mapping()
    {
        return [
            'search_keywords' => 'post_title',
            'search_region' => [
                'taxonomy' => 'job_listing_region',
                'terms' => '',
                'field' => 'term_id',
            ],
        ];
    }

    /**
     * Overriding default query condition
     *
     * @param string $where
     * @param \WP_Query $wp_query
     * @return string
     */
    public function search_condition($where, &$wp_query)
    {
        $mapping = $this->_get_fields_mapping();
        $wp_query->get('search_keywords');
        if ($wp_query->get('search_region')) {
            $wp_query->tax_query->quries[] = ['taxonomy' => 'job_listing_region', 'terms' => $wp_query->get('search_region')];
        }
        if ($wp_query->get('search_categories')) {
            $wp_query->tax_query->quries[] = ['taxonomy' => 'job_listing_category', 'terms' => $wp_query->get('search_categories')];
        }
        $like = $this->_adapter->prepare(' LIKE \'%s\'', '%'.$wp_query->get('search_keywords').'%');
        $where .= ' AND '.$this->_adapter->posts.'.'.$mapping['search_keywords'].$like;
        return $where;
    }

    /**
     * Output search results
     */
    public function search()
    {
        add_filter('posts_where', array(self::get_instance(), 'search_condition'), 10, 2);
        $params = [
            'search_keywords' => !empty($_GET['search_keywords']) ? $_GET['search_keywords'] : null,
            'post_type' => 'job_listing',
            'posts_per_page' => 15,
            'post_status' => 'publish',
            'suppress_filters' => false,
            'order' => 'desc'
        ];

        if (!empty($_GET['search_region'])) {
            $params['tax_query'][] = ['taxonomy' => 'job_listing_region', 'terms' => $_GET['search_region']];
        }

        if (!empty($_GET['search_categories']) && !empty($_GET['search_categories'][0])) {
            $params['tax_query'][] = ['taxonomy' => 'job_listing_category', 'terms' => $_GET['search_categories']];
        }

        $query = new \WP_Query($params);
        if (!$query->have_posts()) {
            wp_die('<div class="no-results">'.__('No results found').'</div>');
        }

        foreach ($query->get_posts() as $post) {
            include $this->_get_template_dir('content-loop');
        }
        wp_die();
    }

    /**
     * Retrieving plugin URL
     *
     * @return string
     */
    protected function _get_plugin_url()
    {
        return dirname(plugins_url(self::$_instance->_plugin_basename));
    }

    /**
     * Retrieving css url
     *
     * @param $filename
     * @return string
     */
    protected function get_css_url($filename)
    {
        return $this->_get_plugin_url().'/assets/css/'.$filename.'.css';
    }

    /**
     * Retrieving css url
     *
     * @param $filename
     * @return string
     */
    protected function get_js_url($filename)
    {
        return $this->_get_plugin_url().'/assets/js/'.$filename.'.js';
    }

    /**
     * Retrieving css url
     *
     * @param $filename
     * @return string
     */
    protected function _get_image_url($filename)
    {
        return $this->_get_plugin_url().'/assets/images/'.$filename;
    }

    /**
     * Retrieving template path
     *
     * @param string $template
     * @return string
     */
    protected function _get_template_dir($template)
    {
        return $this->_plugin_dir.DIRECTORY_SEPARATOR.'templates'.DIRECTORY_SEPARATOR.$template.'.php';
    }

    /**
     * Retrieving terms list
     *
     * @param \WP_Post $post
     * @return mixed|string
     */
    protected function _get_terms_list($post)
    {
        $terms = wp_get_post_terms($post->ID, 'job_listing_region');
        $result = [];
        foreach ($terms as $term) {
            array_push($result, $term->name);
        }
        return implode(',', $result);
    }
}
