(function($){
    var dfSearchField = $(df_ajax_search.selector);
    dfSearchField.replaceWith('<div class="df-search-field"><div class="df-search-results-wrapper"><div class="df-search-results"></div></div></div>');
    jQuery('.df-search-results-wrapper').before(dfSearchField);
    $('body').on('keyup', df_ajax_search.selector, function() {
        if ($(this).val().length > 2) {
            dfSearchField.closest('form').find('.df-search-results-wrapper').addClass('loading').css('display', 'block');
            $.ajax({
                url: df_ajax_search.ajaxurl+'?action=df_search&'+ dfSearchField.closest('form').serialize(),
                type: 'get',
                success: function(response) {
                    dfSearchField.closest('form').find('.df-search-results-wrapper').removeClass('loading');
                    dfSearchField.closest('form').find('.df-search-results').html(response);
                }
            })
        } else {
            dfSearchField.closest('form').find('.df-search-results-wrapper').css('display', 'none');
        }
    })
})(jQuery);

